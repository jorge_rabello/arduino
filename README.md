Repositório com códigos básicos para arduino.

Todos os experimentos:
https://www.tinkercad.com/dashboard?type=circuits&collection=designs

Incluído primeiro exemplo blink:
https://www.tinkercad.com/things/jat4DupQqaw-neat-waasa/editel?tenant=circuits

Incluído experimento 02 - leds em sequencia:
https://www.tinkercad.com/things/kcBfrvYcGTJ-sequenciadeleds/editel

Incluído experimento 03 - led rgb
https://www.tinkercad.com/things/8b3qA22RRPw-ledrgb
