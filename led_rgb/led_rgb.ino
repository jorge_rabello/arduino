void setup() {
  pinMode(3, OUTPUT);
  pinMode(5, OUTPUT);  
  pinMode(6, OUTPUT);  
}

void loop() {
  analogWrite(6, 255);    
  delay(1000);
  analogWrite(6, 0);    
  delay(1000);
  
  analogWrite(5, 255);
  delay(1000);
  analogWrite(5, 0);
  delay(1000);
  
  analogWrite(3, 255);
  delay(1000);
  analogWrite(3, 0);
  delay(1000); 
  
  analogWrite(6, 255);
  analogWrite(5, 255);
  analogWrite(3, 255);
  delay(1000);
  analogWrite(6, 0);
  analogWrite(5, 0);
  analogWrite(3, 0);
  delay(1000);
  
  analogWrite(6, 107); 
  analogWrite(5, 35); 
  analogWrite(3, 142);
  delay(1000);
  analogWrite(6, 0);
  analogWrite(5, 0);
  analogWrite(3, 0);
  delay(1000);
}
